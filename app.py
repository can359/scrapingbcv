from os import system
from bs4 import BeautifulSoup
import pathlib
import urllib.request
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

def notification(type, message):
    """Notification for linux

    Args:
        type (string): Type of icon to show
        message (string): Message to show
    """
    icon = ''
    path = pathlib.Path(__file__).parent.resolve()
    if type == 's':
        icon = f'{path}/assets/sack-dollar-solid.svg'
    else:
        icon = f'{path}/assets/circle-exclamation-solid.svg'

    system(f'notify-send -i {icon} -t 10000 "Precio dolar" "{message}"')

try:
    # Get the web in html
    web = urllib.request.urlopen('http://www.bcv.org.ve/', timeout=10).read().decode()

    # Parse html
    soupHandler = BeautifulSoup(web, 'html.parser')

    # Find the div with id 'dolar'
    divDolar = soupHandler.find('div', id='dolar')

    # Find the div that contain the price, replace characters to become it in float
    dolarCost = float(divDolar.div.div.select('.centrado')[0].strong.get_text().replace(' ', '').replace(',', '.'))

    # Date of price
    day = soupHandler.find('div', class_="dinpro")

    # Build message
    message = f'{day.span.get_text()} - {dolarCost}'

    notification('s', message)
except Exception as e:
    # print(f'Ha ocurrido un error: {e}')
    notification('e', f'Ha ocurrido un error: {e}')