# Scraping Banco Central de Venezuela

_El objetivo de este proyecto esta orientado a realizar un escaneo de la pagina del [BCV](http://www.bcv.org.ve/) y obtener el precio del dolar al momento de ejecucion._

## Prerequisitos

* [BS4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [Python 3.10](https://www.python.org/)
